import SwapHistoryView from "./views/SwapHistoryView";

export default [
  {
    path: "/swap-history",
    name: "swapHistory",
    component: SwapHistoryView
  }
];
