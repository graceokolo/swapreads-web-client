import axios from "axios";
import Vue from "vue";
import { eventBus } from "@/main";

const state = {
  baseURL: process.env.VUE_APP_SWAPREADS_BASE_URL,
  baseResourceURL: process.env.VUE_APP_SWAPREADS_BASE_RESOURCE_URL,
  // requests
  requests: [],
  // swap history
  swapHistory: [],
  userAddresses: undefined
};

const getters = {
  // requests
  requests(state) {
    return state.requests.sort(function(a, b) {
      var createdA = a.created;
      var createdB = b.created;
      if (createdA < createdB) {
        return -1;
      }
      if (createdA > createdB) {
        return 1;
      }
      return 0;
    });
  },
  swapHistory(state) {
    return state.swapHistory;
  },
  userAddresses(state) {
    return state.userAddresses;
  }
};

const mutations = {
  // requests
  setRequests(state, payload) {
    state.requests = payload;
  },
  setSwapHistory(state, payload) {
    state.swapHistory = payload;
  },
  setUserAddresses(state, payload) {
    state.userAddresses = payload;
  }
};

const actions = {
  // requests
  getRequests(context) {
    axios({
      method: "GET",
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}requests/`
    }).then(response => {
      context.commit("setRequests", response.data);
    });
  },
  checkoutRequests(context, payload) {
    var data = {
      book_recipient_address: payload.address
    };

    if (payload.messageToOwner) {
      data["message_to_owner"] = payload.messageToOwner;
    }

    axios({
      method: "POST",
      headers: context.getters.jwt,
      data: data,
      url: `${context.state.baseURL}requests/${payload.request.id}/checkout/`
    })
      .then(response => {
        console.log("Response checkout Requests", response);
        if (response && response.data) {
          context.dispatch("getWishlist");
          context.dispatch("getRequests");
          context.dispatch("getAuthenticatedUser").then(response2 => {
            context.dispatch("setUser", response2.data);
          });
          eventBus.$emit("confirmCheckout");
        } else {
          console.log("An error occured while processing your checkout");

          Vue.toasted.show(`An error occured while processing your checkout.`, {
            theme: "outline",
            position: "bottom-left",
            duration: 5000,
            type: "error",
            icon: "mdi-alert-circle-outline",
            className: "toastedError",
            action: {
              text: "Close",
              onClick: (e, toastObject) => {
                toastObject.goAway(0);
              }
            }
          });
        }
      })
      .catch(error => {
        console.log("An error occured while processing your checkout", error);

        Vue.toasted.show(
          `An error occured while processing your checkout. ${error}`,
          {
            theme: "outline",
            position: "bottom-left",
            duration: 5000,
            type: "error",
            icon: "mdi-alert-circle-outline",
            className: "toastedError",
            action: {
              text: "Close",
              onClick: (e, toastObject) => {
                toastObject.goAway(0);
              }
            }
          }
        );
      });
  },
  declineRequestByRecipient(context, request) {
    var payload = {
      request: {
        id: request.id
      }
    };
    axios({
      method: "DELETE",
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}requests/user/decline/`
    }).then(() => {
      context.dispatch("getRequests");
    });
  },
  declineRequestBySender(context, request) {
    var payload = {
      request: {
        id: request.id
      }
    };
    axios({
      method: "DELETE",
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}requests/user/decline/`
    }).then(() => {
      context.dispatch("getRequests");
      context.dispatch("getBookshelf");
    });
  },
  acceptRequestBySender(context, request) {
    axios({
      method: "POST",
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}requests/${request.id}/accept/`
    }).then(response => {
      var allRequests = context.state.requests;
      allRequests = allRequests.filter(e => e.id != response.data.id);
      allRequests.push(response.data);
      context.commit("setRequests", allRequests);

      // Update bookshelf
      var userBookID = response.data.request_books[0].user_book.id;
      var bookshelf = context.rootState.bookshelf.bookshelf;
      bookshelf = bookshelf.filter(e => e.id != userBookID);
      bookshelf.push(response.data);
      context.commit("setBookshelf", bookshelf);
      return response.data;
    });
  },
  generateShippingLabel(context, request) {
    axios({
      method: "POST",
      headers: context.getters.jwt,
      data: {
        shipping_label_type: "LABEL"
      },
      url: `${context.state.baseURL}requests/${request.id}/shipping/`
    })
      .then(response => {
        context.dispatch("getRequests");
        // window.open(response.data.shipping_label_url, "_blank");
        if (!response) {
          Vue.toasted.show(
            "An error occured while generating your shipping label",
            {
              theme: "outline",
              position: "bottom-left",
              duration: 5000,
              className: "toastedError",
              type: "error",
              icon: "mdi-alert-circle-outline",
              action: {
                text: "Close",
                onClick: (e, toastObject) => {
                  toastObject.goAway(0);
                }
              }
            }
          );
        }
      })
      .catch(error => {
        Vue.toasted.show(
          `An error occured while generating your shipping label. ${error}`,
          {
            theme: "outline",
            position: "bottom-left",
            duration: 5000,
            type: "error",
            icon: "mdi-alert-circle-outline",
            className: "toastedError",
            action: {
              text: "Close",
              onClick: (e, toastObject) => {
                toastObject.goAway(0);
              }
            }
          }
        );
      });
  },
  generateShippingQR(context, request) {
    axios({
      method: "POST",
      headers: context.getters.jwt,
      data: {
        shipping_label_type: "QR"
      },
      url: `${context.state.baseURL}requests/${request.id}/shipping/`
    })
      .then(response => {
        context.dispatch("getRequests");
        if (!response) {
          Vue.toasted.show(
            "An error occured while generating your shipping QR",
            {
              theme: "outline",
              position: "bottom-left",
              duration: 5000,
              className: "toastedError",
              type: "error",
              icon: "mdi-alert-circle-outline",
              action: {
                text: "Close",
                onClick: (e, toastObject) => {
                  toastObject.goAway(0);
                }
              }
            }
          );
        }
      })
      .catch(error => {
        Vue.toasted.show(
          `An error occured while generating your shipping QR. ${error}`,
          {
            theme: "outline",
            position: "bottom-left",
            duration: 5000,
            type: "error",
            icon: "mdi-alert-circle-outline",
            className: "toastedError",
            action: {
              text: "Close",
              onClick: (e, toastObject) => {
                toastObject.goAway(0);
              }
            }
          }
        );
      });
  },
  shipRequest(context, request) {
    var data = request.tracking_number
      ? { tracking_number: request.tracking_number }
      : {};

    axios({
      method: "PATCH",
      headers: context.getters.jwt,
      data: data,
      url: `${context.state.baseURL}requests/${request.id}/ship/`
    })
      .then(response => {
        var allRequests = context.state.requests;
        allRequests = allRequests.filter(e => e.id != response.data.id);
        allRequests.push(response.data);
        context.commit("setRequests", allRequests);
      })
      .catch(error => {
        console.log("An error occured in shipRequest", error);
        Vue.toasted.show(`An error occured while marking as shipped.`, {
          theme: "outline",
          position: "bottom-left",
          duration: 5000,
          type: "error",
          icon: "mdi-alert-circle-outline",
          className: "toastedError",
          action: {
            text: "Close",
            onClick: (e, toastObject) => {
              toastObject.goAway(0);
            }
          }
        });
      });
  },
  receiveRequest(context, request) {
    axios({
      method: "PATCH",
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}requests/${request.id}/receive/`
    }).then(response => {
      var allRequests = context.state.requests;
      allRequests = allRequests.filter(e => e.id != response.data.id);
      allRequests.push(response.data);
      context.commit("setRequests", allRequests);
    });
  },
  getSwapHistory(context, action = null) {
    if (action === "next") {
      axios({
        method: "GET",
        headers: context.getters.jwt,
        data: {},
        url:
          context.swapHistoryNextPage |
          `${context.state.baseURL}requests/swap-history/`
      }).then(response => {
        context.commit("setSwapHistory", response.data);
      });
    } else if (action === "previous") {
      axios({
        method: "GET",
        headers: context.getters.jwt,
        data: {},
        url:
          context.swapHistoryPreviousPage |
          `${context.state.baseURL}requests/swap-history/`
      }).then(response => {
        context.commit("setSwapHistory", response.data);
      });
    } else {
      axios({
        method: "GET",
        headers: context.getters.jwt,
        data: {},
        url: `${context.state.baseURL}requests/swap-history/`
      }).then(response => {
        context.commit("setSwapHistory", response.data);
      });
    }
  },
  getUserAddresses(context) {
    axios({
      method: "GET",
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}user-addresses/`
    }).then(response => {
      context.commit("setUserAddresses", response.data);
    });
  },
  addNewUserAddresses(context, address) {
    return axios({
      method: "POST",
      headers: context.getters.jwt,
      data: address,
      url: `${context.state.baseURL}user-addresses/`
    });
  },
  addToUserAddresses(context, address) {
    /*
    To add an address to the top of userAddress array
    */
    var allAddresses = context.state.userAddresses
      ? context.state.userAddresses
      : [];
    allAddresses = allAddresses.filter(e => e.id != address.id);
    allAddresses.unshift(address);
    context.commit("setUserAddresses", allAddresses);
  },
  editNewUserAddresses(context, address) {
    axios({
      method: "PATCH",
      headers: context.getters.jwt,
      data: address,
      url: `${context.state.baseURL}user-addresses/${address.id}/`
    }).then(response => {
      /*
      To avoild making another call to the database, we will get allAddresses, remove the current instance,
      and set all the other address to is_default false and the add the response, which is the new default
      */
      var allAddresses = context.state.userAddresses;
      allAddresses = allAddresses.filter(e => e.id != response.data.id);
      allAddresses.push(response.data);
      context.commit("setUserAddresses", allAddresses);
    });
  },
  addBookToRequest(context, payload) {
    var data = {
      book: {
        id: payload.bookID
      }
    };
    return axios({
      method: "PATCH",
      headers: context.getters.jwt,
      data: data,
      url: `${context.state.baseURL}requests/${payload.requestID}/add-book/`
    }).then(() => {
      context.dispatch("getRequests");
    });
  },
  removeBookFromRequest(context, payload) {
    var data = {
      book: {
        id: payload.bookID
      }
    };
    return axios({
      method: "DELETE",
      headers: context.getters.jwt,
      data: data,
      url: `${context.state.baseURL}requests/${payload.requestID}/remove-book/`
    }).then(() => {
      context.dispatch("getRequests");
      context.dispatch("getWishlist");
    });
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
