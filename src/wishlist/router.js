import WishlistView from "./views/WishlistView";

export default [
  {
    path: "/wishlist",
    name: "wishlist",
    component: WishlistView
  }
];
