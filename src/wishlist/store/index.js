import axios from "axios";

const state = {
  baseURL: process.env.VUE_APP_SWAPREADS_BASE_URL,
  baseResourceURL: process.env.VUE_APP_SWAPREADS_BASE_RESOURCE_URL,

  // wishlist states
  wishlist: []
};

const getters = {
  // wishlist getters
  wishlist(state) {
    return state.wishlist;
  }
};

const mutations = {
  // wishlist mutations
  setWishlist(state, payload) {
    state.wishlist = payload;
  }
};

const actions = {
  // Wishlist actions
  addToWishlist(context, payload) {
    axios({
      method: "POST",
      headers: context.getters.jwt,
      data: {
        book: {
          id: payload.book.id
        }
      },
      url: `${context.state.baseURL}wishlist/`
    }).then(response => {
      var wishlist = [...context.state.wishlist];
      wishlist.push(response.data);
      context.commit("setWishlist", wishlist);
    });
  },
  removeFromWishlist(context, payload) {
    axios({
      method: "DELETE",
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}wishlist/${payload.id}/`
    }).then(() => {
      var wishlist = [...context.state.wishlist];
      context.commit(
        "setWishlist",
        wishlist.filter(e => e.id != payload.id)
      );
    });
  },
  getWishlist(context) {
    axios({
      method: "GET",
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}wishlist/`
    }).then(response => {
      context.commit("setWishlist", response.data);
    });
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
