import Vue from "vue";
import Vuetify from "vuetify/lib";
// import colors from "vuetify/lib/util/colors";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#22dbcd",
        primaryHover: "#64e4da",
        secondary: "#fc7a7a",
        secondaryHover: "#f78c8c",
        accent: "#f5f5f4"
      }
    }
  }
});
