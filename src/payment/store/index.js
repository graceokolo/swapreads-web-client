import axios from "axios";

const state = {
  baseURL: process.env.VUE_APP_SWAPREADS_BASE_URL
};

const getters = {};

const mutations = {};

const actions = {
  checkoutStripe(context) {
    axios({
      method: "POST",
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}create-checkout-session/`
    }).then(response => {
      var bookshelf = [...context.state.bookshelf];
      bookshelf.push(response.data);
      context.commit("setBookshelf", bookshelf);
    });
  },
  completeSuccessfulPayment(context, sessionID) {
    axios({
      method: "POST",
      headers: context.getters.jwt,
      data: { session_id: sessionID },
      url: `${context.state.baseURL}users-finance/`
    }).then(() => {
      context.dispatch("getAuthenticatedUser").then(response => {
        context.dispatch("setUser", response.data);
      });
    });
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
