import StripeCheckoutView from "./views/StripeCheckoutView";
import StripeSuccessView from "./views/StripeSuccessView";
import StripeCancelView from "./views/StripeCancelView";

export default [
  {
    path: "/checkout",
    name: "checkout",
    component: StripeCheckoutView
  },
  {
    path: "/success",
    name: "success",
    component: StripeSuccessView
  },
  {
    path: "/cancel",
    name: "cancel",
    component: StripeCancelView
  }
];
