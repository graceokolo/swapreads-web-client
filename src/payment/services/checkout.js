import { loadStripe } from "@stripe/stripe-js";
const stripeKey =
  process.env.VUE_APP_NODE_ENV == "production"
    ? process.env.VUE_APP_SWAPREADS_PROD_STRIPE_KEY
    : process.env.VUE_APP_SWAPREADS_STRIPE_KEY;

const stripePromise = loadStripe(stripeKey);

export async function stripeCheckout() {
  // Get Stripe.js instance
  const stripe = await stripePromise;

  // Call your backend to create the Checkout Session
  const response = await fetch(
    `${process.env.VUE_APP_SWAPREADS_BASE_URL}create-checkout-session/`,
    {
      method: "POST",
      headers: this.$store.getters.jwt
    }
  );

  const session = await response.json();

  // When the customer clicks on the button, redirect them to Checkout.
  const result = await stripe.redirectToCheckout({
    sessionId: session.id
  });

  if (result.error) {
    // If `redirectToCheckout` fails due to a browser or network
    // error, display the localized error message to your customer
    // using `result.error.message`.
  }
}
