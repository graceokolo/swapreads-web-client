import Vue from "vue";
import VueRouter from "vue-router";
import Store from "@/store";
import authenticationRoutes from "../authentication/router";
import wishlistRoutes from "../wishlist/router";
import bookshelfRoutes from "../bookshelf/router";
import requestsRoutes from "../requests/router";
import bookRoutes from "../book/router";
import userRoutes from "../user/router";
import paymentRoutes from "../payment/router";

Vue.use(VueRouter);

const baseRoutes = [{ path: "*", redirect: "/" }];

let routes = authenticationRoutes.concat(wishlistRoutes);
routes = routes.concat(bookshelfRoutes);
routes = routes.concat(requestsRoutes);
routes = routes.concat(paymentRoutes);
routes = routes.concat(bookRoutes);
routes = routes.concat(userRoutes);
routes = routes.concat(baseRoutes);

const router = new VueRouter({
  mode: "history",
  routes
});

router.beforeEach((to, from, next) => {
  // const publicPages = ["completeDiscourse"];
  const publicPages = ["login"];
  const authRequired = !publicPages.includes(to.name);
  const loggedIn = Boolean(Store.getters.user);
  console.log("before", to.name);

  if (authRequired && loggedIn) {
    return next();
  } else if (authRequired && !loggedIn) {
    // if (from.name != "home") {
    return next("/login");
    // Store.dispatch("loginWithSSOProvider").catch(error => {
    //   if (!error.response) {
    //     Vue.toasted.show("Network Error", {
    //       theme: "outline",
    //       position: "bottom-left",
    //       duration: 5000,
    //       className: "toastedError",
    //       type: "error",
    //       icon: "mdi-wifi-strength-alert-outline",
    //       action: {
    //         text: "Close",
    //         onClick: (e, toastObject) => {
    //           toastObject.goAway(0);
    //         }
    //       }
    //     });
    //   }
    // });
  } else {
    if (from.name != to.name) {
      return next();
    }
  }
});

export default router;
