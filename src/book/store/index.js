import axios from "axios";

const state = {
  baseURL: process.env.VUE_APP_SWAPREADS_BASE_URL,
  books: [],
  bookStatus: undefined
};

const getters = {
  books(state) {
    return state.books;
  },
  bookStatus(state) {
    return state.bookStatus;
  }
};

const mutations = {
  setBooks(state, payload) {
    state.books = payload;
  },
  setBookStatus(state, payload) {
    state.bookStatus = payload;
  }
};

const actions = {
  getBooks(context, queryString = undefined) {
    if (queryString) {
      context.commit("setBookStatus", "searching");
    }

    var url = queryString
      ? `${context.state.baseURL}books?search=${queryString}`
      : `${context.state.baseURL}books/available/?page_size=100`;

    axios({
      method: "GET",
      headers: context.getters.jwt,
      data: {},
      url: url
    })
      .then(response => {
        context.commit("setBooks", []);
        context.commit("setBooks", response.data.results);

        if (queryString) {
          context.commit("setBookStatus", undefined);
        } else {
          context.commit("setBookStatus", "available");
        }
      })
      .catch(() => {
        context.commit("setBookStatus", undefined);
      });
  },
  getUsersAvailableBooks(context, owner) {
    return axios({
      method: "GET",
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}books/available/owner?id=${owner}&page_size=100`
    });
  },
  getBook(context, id) {
    return axios({
      method: "GET",
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}books/${id}`
    });
  },
  createBook(context, payload) {
    return axios({
      method: "POST",
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}books/`
    });
  },
  updateBook(context, payload) {
    return axios({
      method: "PATCH",
      headers: context.getters.jwt,
      data: payload.data,
      url: `${context.state.baseURL}books/${payload.id}/`
    });
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
