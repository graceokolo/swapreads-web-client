import BooksView from "./views/BooksView";
import BookView from "./views/BookView";

export default [
  {
    path: "/:display",
    name: "searchResult",
    component: BooksView
  },
  {
    path: "/",
    name: "home",
    component: BooksView
  },
  {
    path: "/books/:id",
    name: "book",
    component: BookView
  }
];
