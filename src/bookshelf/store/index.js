import axios from "axios";
// import Router from "@/router";

const state = {
  baseURL: process.env.VUE_APP_SWAPREADS_BASE_URL,
  baseResourceURL: process.env.VUE_APP_SWAPREADS_BASE_RESOURCE_URL,

  // bookshelf states
  bookshelf: [],
  userBookConditions: []
};

const getters = {
  // bookshelf getters
  bookshelf(state) {
    return state.bookshelf;
  },
  userBookConditions(state) {
    return state.userBookConditions;
  }
};

const mutations = {
  // bookshelf mutations
  setBookshelf(state, payload) {
    state.bookshelf = payload;
  },
  setUserBookConditions(state, payload) {
    state.userBookConditions = payload;
  }
};

const actions = {
  // Bookshelf actions
  addToBookshelf(context, payload) {
    payload.book = {
      id: payload.book.id
    };
    axios({
      method: "POST",
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}bookshelf/`
    }).then(response => {
      var bookshelf = [...context.state.bookshelf];
      bookshelf.push(response.data);
      context.commit("setBookshelf", bookshelf);
    });
  },
  removeFromBookshelf(context, payload) {
    axios({
      method: "DELETE",
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}bookshelf/${payload.id}/`
    }).then(() => {
      var bookshelf = [...context.state.bookshelf];
      context.commit(
        "setBookshelf",
        bookshelf.filter(e => e.id != payload.id)
      );
    });
  },
  updateBookshelf(context, payload) {
    axios({
      method: "PATCH",
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}bookshelf/${payload.id}/`
    }).then(response => {
      var bookshelf = [...context.state.bookshelf];
      bookshelf = bookshelf.filter(e => e.id != payload.id);
      bookshelf.push(response.data);
      context.commit("setBookshelf", bookshelf);
    });
  },
  getBookshelf(context) {
    axios({
      method: "GET",
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}bookshelf/`
    }).then(response => {
      context.commit("setBookshelf", response.data);
    });
  },
  getUserBookConditions(context) {
    if (context.state.userBookConditions.length < 1) {
      axios({
        method: "GET",
        headers: context.getters.jwt,
        data: {},
        url: `${context.state.baseResourceURL}user-book-condition/`
      }).then(response => {
        context.commit("setUserBookConditions", response.data);
      });
    }
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
