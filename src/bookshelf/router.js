import BookshelfView from "./views/BookshelfView";

export default [
  {
    path: "/bookshelf",
    name: "bookshelf",
    component: BookshelfView
  }
];
