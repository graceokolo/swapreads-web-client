import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import Toasted from "vue-toasted";

require("dotenv").config();
const amplitude = require("amplitude-js");
amplitude.getInstance().init("process.env.VUE_APP_SWAPREADS_AMPLITUDE_KEY");

Vue.config.productionTip = false;
Vue.use(Toasted, {
  iconPack: "mdi"
});

Vue.use(require("vue-moment"));

export const eventBus = new Vue();

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
