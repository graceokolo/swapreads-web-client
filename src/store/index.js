import Vue from "vue";
import Vuex from "vuex";
import authentication from "../authentication/store";
import wishlist from "../wishlist/store";
import bookshelf from "../bookshelf/store";
import requests from "../requests/store";
import book from "../book/store";
import user from "../user/store";
import payment from "../payment/store";
import VuexPersistence from "vuex-persist";
import { axiosInterceptor } from "../utils/requestInterceptor";

Vue.use(Vuex);

axiosInterceptor();

const vuexLocal = new VuexPersistence({
  key: "vuex",
  storage: window.localStorage
});

export default new Vuex.Store({
  state: {
    communityURL: process.env.VUE_APP_SWAPREADS_DISCOURSE_URL
  },
  getters: {
    communityURL(state) {
      return state.communityURL;
    }
  },
  plugins: [vuexLocal.plugin],
  mutations: {},
  actions: {},
  modules: {
    authentication,
    wishlist,
    bookshelf,
    book,
    user,
    requests,
    payment
  }
});
