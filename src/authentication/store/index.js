import axios from "axios";
import Router from "@/router";
import userflow from "userflow.js";
import Vue from "vue";

const state = {
  baseURL: process.env.VUE_APP_SWAPREADS_BASE_URL,
  loginURL: process.env.VUE_APP_SWAPREADS_BASE_SOCIAL_LOGIN_URL,
  completeURL: process.env.VUE_APP_SWAPREADS_BASE_SOCIAL_COMPLETE_URL,
  users: [],
  user: null,
  jwt: null,
  jwtRefresh: null
};

const getters = {
  users: state => {
    return state.users;
  },
  user: state => {
    return state.user;
  },
  jwt: state => {
    return { Authorization: "Bearer " + state.jwt };
  },
  jwtAccess: state => {
    return state.jwt;
  },
  jwtRefresh: state => {
    return state.jwtRefresh;
  }
};

const mutations = {
  setUsers: (state, payload) => {
    state.users = payload;
  },
  setUser: (state, payload) => {
    state.user = payload;
  },
  setJWT: (state, payload) => {
    state.jwt = payload;
    localStorage.setItem("jwt", "jwt");
  },
  setJWTRefresh: (state, payload) => {
    state.jwtRefresh = payload;
  },
  deleteUser: state => {
    state.user = null;
  },
  deleteJWT: state => {
    state.jwt = null;
  },
  deleteJWTRefresh: state => {
    state.jwtRefresh = null;
  }
};

const actions = {
  login: (context, payload) => {
    return axios.post(`${context.state.baseURL}token/`, {
      username: payload.username,
      password: payload.password
    });
  },
  logout: context => {
    context.dispatch("unauthenticate");
  },
  setUser: (context, payload) => {
    context.commit("setUser", payload);
  },
  setJWT: (context, payload) => {
    context.commit("setJWT", payload);
  },
  setJWTRefresh: (context, payload) => {
    context.commit("setJWTRefresh", payload);
  },
  unauthenticate: context => {
    context.commit("deleteUser");
    context.commit("deleteJWT");
    context.commit("deleteJWTRefresh");
    localStorage.clear();
    window.localStorage.clear();
    window.sessionStorage.clear();
    // window.location.replace("https://swapreads.com/");
    Router.push({ name: "login" });
  },
  getAuthenticatedUser: context => {
    return axios({
      method: "GET",
      headers: context.getters.jwt,
      data: {},
      url: `${context.state.baseURL}users/authenticated-user/`
    });
  },
  updateUser: (context, payload) => {
    return axios({
      method: "PATCH",
      headers: context.getters.jwt,
      data: payload,
      url: `${context.state.baseURL}users/${payload.id}/`
    }).then(response => {
      if (response?.data) context.commit("setUser", response.data);
    });
  },
  loginWithSSOProvider: context => {
    return axios({
      method: "GET",
      url: `${context.state.loginURL}?backend=discourse`
    })
      .then(response => {
        window.location.replace(response.data.redirect_url);
      })
      .catch(error => {
        Vue.toasted.show(`An error occured starting up SSO. ${error}`, {
          theme: "outline",
          position: "bottom-left",
          duration: 5000,
          type: "error",
          icon: "mdi-alert-circle-outline",
          className: "toastedError",
          action: {
            text: "Close",
            onClick: (e, toastObject) => {
              toastObject.goAway(0);
            }
          }
        });
      });
  },
  completeWithSSOProvider: (context, ssoProviderQuery) => {
    return axios({
      method: "GET",
      url: `${context.state.completeURL}?backend=discourse&sso=${ssoProviderQuery.sso}&sig=${ssoProviderQuery.sig}`
    }).then(response => {
      context.commit("setJWT", response.data.token.access);
      context.commit("setJWTRefresh", response.data.token.refresh);
      context.commit("setUser", response.data.user);

      var userflowEnvVal =
        process.env.VUE_APP_NODE_ENV == "production"
          ? process.env.VUE_APP_SWAPREADS_PROD_USER_FLOW_KEY
          : process.env.VUE_APP_SWAPREADS_USER_FLOW_KEY;
      userflow.init(userflowEnvVal);
      userflow.identify(response.data.user.external_id, {
        name: response.data.user.first_name || response.data.user.username,
        signed_up_at: response.data.user.created
      });

      Router.push({ name: "home" });
    });
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
