import CompleteDiscourseService from "./services/CompleteDiscourseService";
import LoginView from "./views/LoginView";

export default [
  {
    path: "/complete/discourse/",
    name: "completeDiscourse",
    component: CompleteDiscourseService
  },
  {
    path: "/login",
    name: "login",
    component: LoginView
  }
];
