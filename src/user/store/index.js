const state = {
  baseURL: process.env.VUE_APP_SWAPREADS_BASE_URL
};

const getters = {};

const mutations = {};

const actions = {};

export default {
  state,
  getters,
  mutations,
  actions
};
