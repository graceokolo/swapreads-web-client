import axios from "axios";
// import router from "@/router";
import store from "../store";

export function axiosInterceptor() {
  var baseURL = process.env.VUE_APP_SWAPREADS_BASE_URL;
  // Add a request interceptor
  axios.interceptors.request.use(
    config => {
      const token = store.getters.jwtAccess;
      if (token) {
        config.headers["Authorization"] = "Bearer " + token;
      }
      return config;
    },
    error => {
      Promise.reject(error);
    }
  );

  //Add a response interceptor
  axios.interceptors.response.use(
    response => response,
    error => {
      const originalRequest = error.config;

      if (
        error.response &&
        (error.response.status === 401 || error.response.status === 400) &&
        originalRequest.url == `${baseURL}token/refresh/`
      ) {
        store.dispatch("unauthenticate");
        // router.push("home");
        return Promise.reject(error);
      }

      if (
        error.response &&
        error.response.status === 401 &&
        !originalRequest._retry
      ) {
        originalRequest._retry = true;
        const refreshToken = store.getters.jwtRefresh;
        return axios
          .post(`${baseURL}token/refresh/`, {
            refresh: refreshToken
          })
          .then(response => {
            if (response.status === 200) {
              store.dispatch("setJWT", response.data.access);
              axios.defaults.headers.common["Authorization"] =
                "Bearer " + response.data.access;
              return axios(originalRequest);
            }
          });
      } else if (
        error.response &&
        error.response.status === 401 &&
        originalRequest._retry
      ) {
        store.dispatch("unauthenticate");
      }

      if (error.response && error.response.status === 410) {
        return Promise.reject(error);
      }

      // store.dispatch("unauthenticate");
      // return Promise.reject(error);
    }
  );
}
