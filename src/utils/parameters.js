export var parameters = {
  bookCondition: {
    good: "Good",
    acceptable: "Acceptable",
    likeNew: "Like New",
    veryGood: "Very Good"
  },
  requestDeclineReason: {
    declinedByUser: "Declined by User",
    timeLimitExceeded: "Time Limit Exceeded"
  },
  userWishStatus: {
    matched: "Matched",
    requested: "Requested",
    unmatched: "Unmatched"
  },
  // Show relevant request status to the Sender and Recipents of books
  requestStatus: {
    open: {
      sender: "Incoming Match",
      recipient: "New Match"
    },
    recipientPaid: {
      sender: "New Request",
      recipient: "Request Sent"
    },
    declined: {
      sender: "Declined",
      recipient: "Declined"
    },
    senderAccepted: {
      sender: "Request Accepted",
      recipient: "Preparing Package"
    },
    shippingLabelPrinted: {
      sender: "Shipping Label Generated",
      recipient: "Shipping Label Printed"
    },
    shipped: {
      sender: "Shipped",
      recipient: "Shipped"
    },
    recipientReceived: {
      sender: "Received",
      recipient: "Received"
    }
    // bookshelf2HoursReminderSent: {
    //   sender: "2 hours left reminder",
    //   recipient: "Reminder sent to sender"
    // },
    // bookshelf16HoursReminderSent: {
    //   sender: "16 hours left reminder",
    //   recipient: "Reminder sent to sender"
    // },
    // wishlist16HoursReminderSent: {
    //   sender: "Reminder sent to recipient",
    //   recipient: "16 hours left reminder"
    // },
    // wishlist2HoursReminderSent: {
    //   sender: "Reminder sent to recipient",
    //   recipient: "2 hours left reminder"
    // }
  }
};

// Region UserWish Status
export const userWishStatusUnmatched = "unmatched";
export const userWishStatusRequested = "requested";
export const userWishStatusMatched = "matched";

// Region Request Status
export const requestStatusOpen = "open";
export const requestStatusRecipientPaid = "recipientPaid";
export const requestStatusSenderAccepted = "senderAccepted";
export const requestStatusShippingLabelPrinted = "shippingLabelPrinted";
export const requestStatusShipped = "shipped";
export const requestStatusDeclined = "declined";
export const requestrRecipientReceived = "recipientReceived";
